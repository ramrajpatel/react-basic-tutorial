import React, {Component} from 'react';
import { forInStatement } from '@babel/types';

class Form extends Component{

    constructor(props){
        super(props)

        this.initialState = {
            name: '',
            job: ''
        }

        this.state = this.initialState
        
    }

    handleChange = (event) => {
        const {name, value} = event.target

        this.setState({
            [name]: value
        })
    }
    
    render = () => {
        const {name, job} = this.state

        return(
            <form>
                <label>Name</label>
                <input type="text" name="name" value={name} onChange={this.handleChange} />

                <label>Job</label>
                <input type="text" name="job" value={job} onChange={this.handleChange} />

                <button type="button" onClick={this.submitForm}>Submit</button>
            </form>
        )
    }

    submitForm = () =>{
        this.props.handleFormSubmit(this.state)
        // this.props.handleFormSubmit({
        //     "name": this.state.name,
        //     "job": this.state.job
        // })
        this.setState(this.initialState)
    }
}

export default Form; 