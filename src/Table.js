import React, {Component} from 'react';
import TableHeader from './TableHeader';
import TableBody from './TableBody';

class Table extends Component {
  render() {
    const { jsonData } = this.props
    return (
      <table>
        <TableHeader />
        <TableBody jsonData={jsonData} removeRow={this.props.removeRow}/>
      </table>
    )
  }
}

export default Table;
