import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import Table from './Table';
import Form from './Form';

class App extends Component {

  state = {
    jsonData: [
      {
        name: 'Charlie',
        job: 'Janitor',
      },
      {
        name: 'Mac',
        job: 'Bouncer',
      },
      {
        name: 'Dee',
        job: 'Aspring actress',
      },
      {
        name: 'Dennis',
        job: 'Bartender',
      },{
        name: 'Ramraj',
        job: 'Dev Team',
      }
    ]
  }

  render = () => {
    const {jsonData} = this.state;
    return (
      <div className="container">
        <Table jsonData={jsonData} removeRow={this.removeRow} />
        <Form handleFormSubmit={this.handleFormSubmit} />
      </div>
    )
  }

  removeRow = index => {
    const {jsonData} = this.state;
  
    this.setState({
      jsonData: jsonData.filter((row, i) => {
        return i !== index
      })
    })
  }

  handleFormSubmit = formData => {
    this.setState({
      jsonData: [...this.state.jsonData, formData]
    })
  }
  
}

export default App;
